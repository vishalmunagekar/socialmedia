import { Component } from '@angular/core';
import { DataService } from './data.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Post } from 'src/app/interface/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  fileData:File;
  previewUrl:any;
  uploadedFilePath:string;
  fileUploadProgress:String;

  constructor(public dataService:DataService){}

  fileProgress(fileInput: any) {
    console.log(fileInput);
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

   preview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = () => {
      this.previewUrl = reader.result;
    }
}

  onSubmit(){
    const formData = new FormData();
    formData.append('image', this.fileData)
    this.fileUploadProgress = '0%';

    this.dataService.imageUpload(formData).subscribe((events:any)=>{
      if(events.type === HttpEventType.UploadProgress){
        this.fileUploadProgress = Math.round(events.loaded / events.total * 100 ) + '%';
      }
      if(events.type === HttpEventType.Response){
        console.log("upload completed");
        console.log(events.body.ImgUrl);
        this.fileUploadProgress = '';
        let newPost:Post = new Post();
        newPost.imgUrl = events.body.ImgUrl;
        newPost.username = "vishal";
        newPost.caption = "Some Random Text!!!";
        this.dataService.SendPost(newPost).subscribe((result:any)=>{
          console.log(result);
          console.log("All Done....");
        })
      }

    })
  }
}
