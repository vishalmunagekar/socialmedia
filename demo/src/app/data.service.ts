import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Post } from './interface/post';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  baseUrl:String = "http://192.168.43.172:9090/";
  constructor( public httpClient:HttpClient) {}


  imageUpload(image:any){
   return this.httpClient.post(
      'http://localhost:3000/image/upload',image, {reportProgress:  true, observe: 'events'}
  )}

  SendPost(post:Post){
    return this.httpClient.post('http://localhost:3000/image/newpost',post );
  }

  testHello(){
    return this.httpClient.get(this.baseUrl + "api/test/hello");
  }

  PoojasMethod1(){
    return this.httpClient.get("http://data.fixer.io/api/2017-01-01?access_key=401b813f53bc48488e308cb9a13e06fb");
  }

  PoojasMethod2(){
    return this.httpClient.get("http://data.fixer.io/api/2017-01-01?access_key=401b813f53bc48488e308cb9a13e06fb");
  }
}
