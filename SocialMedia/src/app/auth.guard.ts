import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  isLogin:boolean = false;

  constructor(private authService:AuthService,private router:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!this.checkLoginStatus()){
        return this.router.navigate(['/login'])
      }
      this.authService.subscriptionForLogin.next(true);
      return true;
  }

  checkLoginStatus():boolean{
    if(localStorage.username !== undefined)
        return true;
    return false;
  }
}
