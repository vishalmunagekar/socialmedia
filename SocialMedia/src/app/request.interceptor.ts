import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { AuthenticationResponse } from './interface/auth';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (
      req.url.indexOf('login') !== -1 || req.url.indexOf('signup') !== -1 || req.url.indexOf('refresh') !== -1 ||
      req.url.indexOf('image') !== -1 // for by pass node JS server to add new post
    ) {
      console.log('bypass....');
      return next.handle(req);
    }
    let JwtToken: string = localStorage.authenticationToken;
    if (JwtToken) {
      return next.handle(this.addTokenToHeader(req, JwtToken)).pipe(
        catchError((error) => {
          console.log('inside interceptor catchError...'); /////////
          if (error instanceof HttpErrorResponse && error.status === 403) {
            console.log('inside 403 error...'); //////////
            return this.handleJwtError(req, next);
          } else {
            throwError(error);
            console.log('inside interceptor catchError else part...'); ///////////
          }
        })
      );
    }
    return next.handle(req);
  }

  handleJwtError(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log('inside Handle...');
    return this.authService.refreshToken().pipe(
      switchMap((authResponse: AuthenticationResponse) => {
        localStorage.setItem(
          'authenticationToken',
          authResponse.authenticationToken
        );
        localStorage.setItem('expiresAt', authResponse.expiresAt);
        return next.handle(
          this.addTokenToHeader(req, authResponse.authenticationToken)
        );
      })
    );
  }

  addTokenToHeader(req: HttpRequest<any>, JwtToken: string) {
    return req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + JwtToken),
    });
  }
}
