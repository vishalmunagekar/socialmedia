import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/services/post.service';
import { Post, PostComments, CommentRequest, Commet } from 'src/app/interface/post';

@Component({
  selector: 'app-detailed-post',
  templateUrl: './detailed-post.component.html',
  styleUrls: ['./detailed-post.component.css'],
})
export class DetailedPostComponent implements OnInit {
  postId: number = parseInt(this.route.snapshot.paramMap.get('postId'));
  detailedPost: Post;
  postComments: PostComments
  constructor(
    private route: ActivatedRoute,
    private postService: PostService
  ) {}

  ngOnInit(): void {
    this.postService.getPostByPostId(this.postId).subscribe((result:Post)=>{
      this.detailedPost = result
    })

  }
}




// const promis = this.postService.getPostByPostId(this.postId).toPromise();
//     //console.log(promis);
//     promis.then(
//       (data: Post) => {
//         this.detailedPost = data;
//         this.postService.getPostCommentsByPostCommentId(data.postCommentId)
//                         .subscribe((comments: PostComments) => {
//             this.postComments = comments
//             this.postComments.commets.reverse()
//           });
//       },
//       (error) => {
//         console.log(error);
//       }
//     );
