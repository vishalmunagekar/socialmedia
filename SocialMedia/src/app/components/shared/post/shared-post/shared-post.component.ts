import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/interface/post';

@Component({
  selector: 'app-shared-post',
  templateUrl: './shared-post.component.html',
  styleUrls: ['./shared-post.component.css']
})
export class SharedPostComponent implements OnInit {
  @Input() singlePost: Post;
  constructor() { }

  ngOnInit(): void {
  }

}
