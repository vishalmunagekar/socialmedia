import { Component, OnInit, Input } from '@angular/core';
import { Commet, PostComments } from 'src/app/interface/post';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
})
export class CommentsComponent implements OnInit {
  @Input() postCommentId: string;
  postComments: PostComments;
  constructor(
    private route: ActivatedRoute,
    private postService: PostService
  ) {}

  ngOnInit(): void {
    this.postService.refreshSubject.subscribe(()=>{
      this.getAllComments()
    })

    this.getAllComments();

  }

  getAllComments(){
  this.postService.getPostCommentsByPostCommentId(this.postCommentId)
                  .subscribe((comments:PostComments) => {
            this.postComments = comments;
            this.postComments.commets.reverse();
      });
  }
}
