import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnkownFriendComponent } from './unkown-friend.component';

describe('UnkownFriendComponent', () => {
  let component: UnkownFriendComponent;
  let fixture: ComponentFixture<UnkownFriendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnkownFriendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnkownFriendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
