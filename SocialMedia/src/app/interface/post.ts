export interface UserProfile {
  userProfileId: number;
  profilePicture: string;
}

export class Post {
  username: string;
  imageUrl: string;
  caption: string;
  postCommentId: string;
  postId: number;
  publishedDate: number;
  userProfile: UserProfile[];
}

export class CommentRequest {
  comment:string;
  username:string;
  postCommentId:string;
}


export class Commet {
  comment: string;
  username: string;
  published: number;
}

export class PostComments {
  postCommentId: string;
  commets: Commet[];
}
