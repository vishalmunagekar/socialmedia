var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var imageUploadRouter = require('./imageUpload');

let port = 3000;

var app = express();
app.use('/resources',express.static('./images'));

app.use(bodyParser.json())

app.use(cors());



app.use('/image', imageUploadRouter);

app.listen(port, function () {
    console.log("Server started on port : " + port);
})